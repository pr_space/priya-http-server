const fs = require('fs');
const http = require('http');
const uuid4 = require('uuid4');
const path = require('path');
const { join } = require('path');


const port = process.env.PORT || 8000;



const server = http.createServer((request, response) => {
  
  const url = request.url;

  switch (url) {
    case '/':

      response.writeHead(200, {
        'content-type' : 'text/html'
      });
      response.write('<h1> Home </h1>');
      response.end();

    break;
    
    case '/html':

      fs.readFile('./inputs/1.html', 'utf-8', function(err,data) {
        if (err) {

          response.writeHead(400,{
            'content-type': 'text/html'
          });
          response.write('<h1>No HTML found</h1>')
          response.end();

        } else {

          response.writeHead(200, {
            'content-type': 'text/html'
          });
          response.write(data);
          response.end();

        }

      });

    break;

    case '/json':

      fs.readFile('./inputs/2.json', 'utf-8', function(err,data){
        if(err) {

          response.writeHead(400, {
            'content-type': 'text.html'
          });
          response.write('<h1>No JSON found.</h1>');
          response.end();


        } else {

          response.writeHead(200, {
            'content-type' : 'application/json'
          });
          response.write(data);
          response.end();

        }

      });

    break;

    case '/uuid':
      response.writeHead(200, {
        'content-type' : 'application/json'
      });
      const id = uuid4()
      response.write(id);
      response.end();


    break;

    case (path.startsWith('/status/')):
      const status = path.slice(8,path.length);
      console.log(status);
      response.write(status);
      response.end();
    break;

    case (path.startsWith('/delay/')):
      const delay = parseInt(path.slice(7,path.length))
      setTimeout(()=>{
          response.write(delay.toString());
          response.end();
      }, delay*1000);



    default: 
    response.writeHead(404, {
      'content-type' : 'text/html'
    });

    response.write("404 Not Found");
    response.end();

  }

})

server.listen(port, () => {
  console.log(`Server running at port ${port}`)
})














